package com.videxedge.realtimedb;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    EditText eTclass, eTgrade;
    ListView lvdisplay;

    String course, str1, str2;
    int grade;
    ArrayList<String> itemlist;
    ArrayAdapter<String> adap;

    DatabaseReference fbdbRootRef= FirebaseDatabase.getInstance().getReference();
    DatabaseReference fbdbCondRef=fbdbRootRef.child("Grades");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        eTclass=(EditText)findViewById(R.id.eTclass);
        eTgrade=(EditText)findViewById(R.id.eTgrade);
        lvdisplay=(ListView)findViewById(R.id.lvdisplay);

        lvdisplay.setOnItemClickListener(this);
        itemlist=new ArrayList<>();
        adap=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, itemlist);
        lvdisplay.setAdapter(adap);

        fbdbCondRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                str1=dataSnapshot.getKey();
                str2=dataSnapshot.getValue().toString();
                itemlist.add(str1+": "+str2);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

    }

    public void datain(View view) {
        course=eTclass.getText().toString();
        str1=eTgrade.getText().toString();
        grade=Integer.parseInt(str1);

//        fbdbCondRef.child(course).push().setValue(grade);

        fbdbCondRef.child(course).push();
        fbdbCondRef.child(course).setValue(grade);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }
}
